﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlgorithmGenetic
{
    class Processing
    {
        private readonly double _probabilityCrossing;
        private readonly double _probabilityMutation;
        private Random random = new Random();

        public Processing(double probabilityCrossing, double probabilityMutation)
        {
            _probabilityCrossing = probabilityCrossing;
            _probabilityMutation = probabilityMutation;
        }

        public Genotype[] DoCrossing(Genotype genotype1, Genotype genotype2, int indexOfCrossing)
        {
            Genotype[] genotypes = {
                genotype1,
                genotype2
            };
            double randomProbability = random.NextDouble();
            if (randomProbability <= _probabilityCrossing)
            {
                Genotype crossedGenotype1 = GetCrossingGenotype(genotype1, genotype2, indexOfCrossing);
                Genotype crossedGenotype2 = GetCrossingGenotype(genotype2, genotype1, indexOfCrossing);

                genotypes = new[]
                {
                    crossedGenotype1,
                    crossedGenotype2
                };

                return genotypes;
            }

            return genotypes;
        }

        public void DoMutation(Genotype genotype)
        {
            double randomProbability = random.NextDouble();
            if (randomProbability < _probabilityMutation)
            {
                int indexToExchange = random.Next(0, genotype.Genes.Count);
                int indexToExchange2 = 0;
                while ((indexToExchange2 = random.Next(0, genotype.Genes.Count)) == indexToExchange) ;

                var temp = genotype.Genes[indexToExchange];
                genotype.Genes[indexToExchange] = genotype.Genes[indexToExchange2];
                genotype.Genes[indexToExchange2] = temp;

            }

        }

        private Genotype GetCrossingGenotype(Genotype g1, Genotype g2, int indexOfCrossing)
        {
            List<int> newGenes = g1.Genes.Take(indexOfCrossing).ToList();
            List<int> newGenesG2 =
                g2.Genes.Skip(indexOfCrossing).Take(g2.Genes.Count - indexOfCrossing).ToList();
            newGenes.AddRange(newGenesG2);

            Genotype newGenotype = new Genotype(g1.LenghtOfGenotype);
            newGenotype.Genes = GetFixedGenes(newGenes, g1.LenghtOfGenotype);

            return newGenotype;
        }

        private List<int> GetFixedGenes(List<int> genes, int maxLength)
        {

            List<int> indexesToFix = new List<int>();
            List<int> uniqueGenes = new List<int>();

            for (int i = 0; i < genes.Count; i++)
            {
                if (!uniqueGenes.Contains(genes[i]))
                {
                    uniqueGenes.Add(genes[i]);
                }
                else
                {
                    indexesToFix.Add(i);
                }
            }

            if (indexesToFix.Count > 0)
            {
                List<int> fixedGenes = genes;
                int randomGen = 0;
                for (int i = 0; uniqueGenes.Count < genes.Count;)
                {
                    randomGen = random.Next(1, maxLength + 1);
                    if (!uniqueGenes.Contains(randomGen))
                    {
                        uniqueGenes.Add(randomGen);
                        fixedGenes[indexesToFix[i]] = randomGen;
                        i++;
                    }
                }

                return fixedGenes;
            }
            return genes;
        }
    }
}
