﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace AlgorithmGenetic
{
    class DataReader
    {
        private StreamReader _streamReader;
        private int[,] _matrixDistance;
        private int[,] _matrixFlow;
        public string Path { get; set; }
        public int[,] MatrixDistance => _matrixDistance ?? (_matrixDistance = GetMatrixDistance());
        public int[,] MatrixFlow => _matrixFlow ?? (_matrixFlow = GetMatrixFlow());

        public DataReader(string path)
        {
            Path = path;
        }

        private int[,] GetMatrixDistance()
        {
            using (_streamReader = new StreamReader(Path, Encoding.UTF8))
            {
                int lenghtOfSquareMatrix = Int32.Parse(_streamReader.ReadLine() ?? "0");
                var distanceArray = new int[lenghtOfSquareMatrix, lenghtOfSquareMatrix];
                string line = _streamReader.ReadLine(); //empty string
                for (int counter = 0;
                    (line = _streamReader.ReadLine()) != null
                    && counter < lenghtOfSquareMatrix;
                    counter++)
                {
                    string[] tempArray = GetNumbersFromString(line);
                    for (int j = 0; j < tempArray.Length; j++)
                        distanceArray[counter, j] = Int32.Parse(tempArray[j]);
                }

                return distanceArray;
            }
        }

        private int[,] GetMatrixFlow()
        {
            using (_streamReader = new StreamReader(Path, Encoding.UTF8))
            {
                int lenghtOfSquareMatrix = Int32.Parse(_streamReader.ReadLine() ?? "0");
                int lengthToSkip = 2 + lenghtOfSquareMatrix; // 2 indexes to skip because file has 2 empty lines
                var flowArray = new int[lenghtOfSquareMatrix, lenghtOfSquareMatrix];

                string line;
                for (int i = 0; i < lengthToSkip; i++)
                    _streamReader.ReadLine();

                for (int counter = 0;
                    (line = _streamReader.ReadLine()) != null
                    && counter < lenghtOfSquareMatrix;
                    counter++)
                {
                    string[] tempArray = GetNumbersFromString(line);
                    for (int j = 0; j < tempArray.Length; j++)
                        flowArray[counter, j] = Int32.Parse(tempArray[j]);
                }

                return flowArray;
            }
        }


        private string[] GetNumbersFromString(string temp)
        {
            return Regex.Split(temp.Trim(), @"\D+");
        }
    }
}
