﻿using System;
using System.Collections.Generic;

namespace AlgorithmGenetic
{
    class Genotype
    {
        public List<int> Genes { get; set; }
        public int LenghtOfGenotype { get; set; }
        public int ValueOfResult { get; set; }

        public Genotype(int lenghtOfGenotype)
        {
            LenghtOfGenotype = lenghtOfGenotype;
            Genes = new List<int>(LenghtOfGenotype);
        }

        public Genotype(Genotype genotype)
        {
            Genes = new List<int>();
            AddNumbers(genotype.Genes);
            LenghtOfGenotype = genotype.LenghtOfGenotype;
            ValueOfResult = genotype.ValueOfResult;
        }

        private void AddNumbers(List<int> numbers)
        {
            foreach (var number in numbers)
                Genes.Add(number);
        }

        public void EvaluateMarkResult(int[,] matrixDistance, int[,] matrixFlow)
        {
            int result = 0;
            for (int i = 0; i < LenghtOfGenotype; i++)
            {
                for (int j = 0; j < LenghtOfGenotype; j++)
                {
                    try
                    {
                        result +=
                            matrixDistance[i, j] *
                            matrixFlow[Genes[i] - 1, Genes[j] - 1];
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            ValueOfResult = result;
        }
    }
}
