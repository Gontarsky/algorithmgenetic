﻿using System;
using System.Collections.Generic;

namespace AlgorithmGenetic
{
    class Population
    {
        public int Size { get; set; }
        public List<Genotype> Genotypes { get; set; }
        public DataReader DataReader { get; set; }
        private Random random = new Random();

        public Population(int size, DataReader dataReader)
        {
            Size = size;
            Genotypes = new List<Genotype>(Size);
            DataReader = dataReader;
        }

        public void AddToPopulation(Genotype genotype)
        {
            Genotype newGenotype = new Genotype(genotype);
            newGenotype.EvaluateMarkResult(DataReader.MatrixDistance, DataReader.MatrixFlow);
            Genotypes.Add(newGenotype);
        }

        public void GeneratePopulation(int lenghtOfGenotype)
        {
            for (int i = 0; i < Size; i++)
            {
                Genotype genotype = new Genotype(lenghtOfGenotype);
                for (; genotype.Genes.Count < lenghtOfGenotype;)
                {
                    var temp = random.Next(1, lenghtOfGenotype + 1);
                    if (!genotype.Genes.Contains(temp))
                    {
                        genotype.Genes.Add(temp);
                    }
                }

                genotype.EvaluateMarkResult(DataReader.MatrixDistance, DataReader.MatrixFlow);
                Genotypes.Add(genotype);

            }
        }
    }
}
