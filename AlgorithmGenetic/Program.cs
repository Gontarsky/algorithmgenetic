﻿using System;
using System.IO;
using System.Threading;

namespace AlgorithmGenetic
{
    class Program
    {
        static void Main(string[] args)
        {
            string _path =
                @"C:\Users\Liberator\Documents\Visual Studio 2017\Projects\SI\1\1\algorithmgenetic\AlgorithmGenetic\Results.csv";
            const int LengthOfGenotype = 12;
            const int SizeOfPopulation = 100;
            const int SizeOfGeneration = 100;
            const int SizeOfTour = 10;
            const double ProbabilityCrossing = 0.7;
            const double ProbabilityMutation = 0.3;
            const int IndexOfCrossing = 5;
            ExitExcelIfOpen();
            File.WriteAllText(_path, "");

            DataReader dataReader =
                new DataReader(
                    @"C:\Users\Liberator\Documents\Visual Studio 2017\Projects\SI\1\1\algorithmgenetic\AlgorithmGenetic\had12.dat");
            Population population = new Population(SizeOfPopulation, dataReader);

            population.GeneratePopulation(LengthOfGenotype);
            Generation generation = new Generation(population, SizeOfGeneration);

            ModelSelection modelSelection = new ModelSelection();
            ModelSelection modelSelection2 = new ModelSelection();

            Processing processing = new Processing(ProbabilityCrossing, ProbabilityMutation);

            for (int i = 0; i < generation.Size; i++)
            {
                Population newPopulation =
                    new Population(SizeOfPopulation, dataReader);
                while (newPopulation.Genotypes.Count < SizeOfPopulation)
                {
                    Genotype genotype1 =
                        new Genotype(generation.Population.Genotypes[
                            modelSelection.ChooseByTour(generation.Population.Genotypes, SizeOfTour)]);
                    Genotype genotype2 =
                        new Genotype(generation.Population.Genotypes[
                            modelSelection2.ChooseByTour(generation.Population.Genotypes, SizeOfTour)]);
                    var genotypesAfterCrossing =
                        processing.DoCrossing(genotype1, genotype2, IndexOfCrossing); // krzyżowanie
                    processing.DoMutation(genotypesAfterCrossing[0]);
                    processing.DoMutation(genotypesAfterCrossing[1]);

                    newPopulation.AddToPopulation(genotypesAfterCrossing[0]);
                    newPopulation.AddToPopulation(genotypesAfterCrossing[1]);

                } // Productivity Power Tools

                generation = new Generation(newPopulation, SizeOfGeneration);
                generation.SaveResultsToFile(i);
            }
        }

        private static void ExitExcelIfOpen()
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                        Thread.Sleep(500);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }
    }
}
