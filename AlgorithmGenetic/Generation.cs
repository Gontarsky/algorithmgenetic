﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AlgorithmGenetic
{
    class Generation
    {
        public Population Population { get; set; }
        public int Size { get; set; }

        private string _path =
            @"C:\Users\Liberator\Documents\Visual Studio 2017\Projects\SI\1\1\algorithmgenetic\AlgorithmGenetic\Results.csv";

        public Generation(Population population, int sizeOfGeneration)
        {
            Population = new Population(sizeOfGeneration, population.DataReader);
            for (int i = 0; i < population.Genotypes.Count; i++)
            {
                Population.Genotypes.Add(new Genotype(population.Genotypes[i]));
            }

            Size = sizeOfGeneration;
        }

        public void SaveResultsToFile(int numberOfGeneration)
        {
            string message = $"{GetBestResult()}, {GetAverageResult()}, {GetWorstResult()}";
            File.AppendAllText(_path, message + Environment.NewLine);
        }

        private double GetBestResult()
        {
            return Population.Genotypes.Min(x => x.ValueOfResult);
        }

        private double GetAverageResult()
        {
            return Population.Genotypes.Average(x => x.ValueOfResult);
        }

        private double GetWorstResult()
        {
            return Population.Genotypes.Max(x => x.ValueOfResult);
        }
    }
}
