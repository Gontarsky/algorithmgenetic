﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlgorithmGenetic
{
    class ModelSelection
    {
        private Random random = new Random();
        public Genotype ChooseByRoulette(List<Genotype> genotypes)
        {
            int earliersValues = 0;
            int sumTotal = genotypes.Sum(x => x.ValueOfResult);
            int[] partialProbability = new int[genotypes.Count];
            for (int i = 0; i < genotypes.Count; i++)
            {
                partialProbability[i] = earliersValues + genotypes[i].ValueOfResult;

                earliersValues = partialProbability[i];
            }

            Genotype randomGenotype = GetRandomGenotype(partialProbability, genotypes, sumTotal);
            return randomGenotype;
        }

        private Genotype GetRandomGenotype(int[] values, List<Genotype> genotypes, int sumTotal)
        {
            for (int j = 0; j < genotypes.Count; j++)
            {
                int randomNumber = random.Next(0, sumTotal);
                for (int i = 0; i < values.Length; i++)
                {
                    if (values[i] > randomNumber)
                        return genotypes[i];

                }
            }

            return null;
        }

        public int ChooseByTour(List<Genotype> genotypes, int sizeOfTour) //model selection allows choose
        {                                                                 //the best genotype who has 
            int[] tab = new int[sizeOfTour];                              //minimum value evaluation
                                                                          //Functiom ChooseByTour return 
                                                                          //best index among genotypes in tour
            for (int i = 0; i < sizeOfTour; i++)
            {
                tab[i] = random.Next(0, genotypes.Count);
            }

            Genotype bestGenotype = genotypes[tab[0]];
            int bestIndex = tab[0];
            for (int i = 1; i < sizeOfTour; i++)
            {
                if (genotypes[tab[i]].ValueOfResult < bestGenotype.ValueOfResult)
                {
                    bestIndex = tab[i];
                }
            }

            return bestIndex;
        }

    }
}
